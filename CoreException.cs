﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace vJine.Core {
    /// <summary>
    /// 表示在调用vJine.Core期间发生的错误
    /// </summary>
    public class CoreException : Exception {
        /// <summary>
        /// 实例化异常实例
        /// </summary>
        public CoreException()
            : base() {

        }
        /// <summary>
        /// 实例化异常
        /// </summary>
        /// <param name="ex">导致当前异常的异常</param>
        public CoreException(Exception ex)
            : base("", ex) {

        }
        /// <summary>
        /// 实例化异常实例
        /// </summary>
        /// <param name="Msg">异常消息</param>
        /// <param name="Args">异常消息参数</param>
        public CoreException(string Msg, params object[] Args)
            : base(string.Format(Msg, Args)) {
        }
        /// <summary>
        /// 实例化异常实例
        /// </summary>
        /// <param name="ex">导致当前异常的异常</param>
        /// <param name="Msg">异常消息</param>
        /// <param name="Args">异常消息参数</param>
        public CoreException(Exception ex, string Msg, params object[] Args)
            : base(string.Format(Msg, Args), ex) {
        }
        /// <summary>
        /// 实例化异常实例
        /// </summary>
        /// <param name="method">产生此异常时的方法</param>
        /// <param name="Msg">异常消息</param>
        /// <param name="Args">异常消息参数</param>
        public CoreException(MethodBase method, string Msg, params object[] Args)
            : base(string.Format(Msg, Args)) {
        }

        /// <summary>
        /// 取消异常
        /// </summary>
        public static readonly CoreException Cancel = new CoreException();
    }
}
