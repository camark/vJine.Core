﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.IoC.Config {
    [AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class ReturnAttribute : ParamAttribute {
        public ReturnAttribute(string Name)
            : this(Name, null) {
        }

        public ReturnAttribute(string Name, string Return)
            : base(Name) {
            this.Return = Return;
        }

        public string Return { get; private set; }
    }
}