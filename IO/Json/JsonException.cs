﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.IO.Json {
    public class JsonException : Exception {
        public JsonException() {
        }

        public JsonException(Exception ex)
            : base(ex.Message, ex.InnerException) {
        }

        public JsonException(string Msg, params object[] Args)
            : base(string.Format(Msg, Args)) {

        }

        public JsonException(Exception inner, string Msg, params object[] Args)
            : base(string.Format(Msg, Args), inner) {
        }
    }
}
