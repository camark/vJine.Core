﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using vJine.Core.IoC;

namespace vJine.Core.IO.Bin {

    static class BinHelperCache {

        public static readonly Dictionary<Type, MethodInfo> _getBytes = new Dictionary<Type, MethodInfo>();
        public static readonly Dictionary<Type, MethodInfo> _getValue = new Dictionary<Type, MethodInfo>();
        static BinHelperCache() {
            Init_Cache();
        }

        #region GetValue

        public static MethodInfo GetValue(Type _t) {
            if (!_getValue.ContainsKey(_t)) {
                return null;
            }

            return _getValue[_t];
        }

        static readonly byte[] B_TRUE = new byte[] { 1 };
        static readonly byte[] B_FALSE = new byte[] { 0 };
        static readonly byte[] B_NULL = new byte[] { 1 };
        static readonly byte[] B_NOT_NULL = new byte[] { 0 };
        static void Init_Cache() {

            //bool
            _getBytes.Add(Reflect.@bool, new Exec<bool, Stream>(
                (bool v, Stream stream) => {
                    if (v) {
                        stream.Write(B_TRUE, 0, 1);
                    } else {
                        stream.Write(B_FALSE, 0, 1);
                    }
                }).Method);
            _getValue.Add(Reflect.@bool, new Call<bool, Stream>((Stream stream) => {
                byte[] B = new byte[1];
                Utility.Read(stream, B, 1);
                return BitConverter.ToBoolean(B, 0);
            }).Method);

            //sbyte
            _getBytes.Add(Reflect.@sbyte, new Exec<sbyte, Stream>((sbyte v, Stream stream) => {
                byte[] B = new byte[] { (byte)v };
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@sbyte, new Call<sbyte, Stream>((Stream stream) => {
                byte[] B = new byte[1];
                Utility.Read(stream, B, 1);
                return (sbyte)B[0];
            }).Method);

            //byte
            _getBytes.Add(Reflect.@byte, new Exec<byte, Stream>((byte v, Stream stream) => {
                byte[] B = new byte[] { v };
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@byte, new Call<byte, Stream>((Stream stream) => {
                byte[] B = new byte[1];
                Utility.Read(stream, B, 1);
                return B[0];
            }).Method);

            //short
            _getBytes.Add(Reflect.@short, new Exec<short, Stream>((short v, Stream stream) => {
                byte[] B = BitConverter.GetBytes(v);
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@short, new Call<short, Stream>((Stream stream) => {
                byte[] B = new byte[2];
                Utility.Read(stream, B, 2);
                return BitConverter.ToInt16(B, 0);
            }).Method);

            //ushort
            _getBytes.Add(Reflect.@ushort, new Exec<ushort, Stream>((ushort v, Stream stream) => {
                byte[] B = BitConverter.GetBytes(v);
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@ushort, new Call<ushort, Stream>((Stream stream) => {
                byte[] B = new byte[2];
                Utility.Read(stream, B, 2);
                return BitConverter.ToUInt16(B, 0);
            }).Method);

            //int
            _getBytes.Add(Reflect.@int, new Exec<int, Stream>((int v, Stream stream) => {
                byte[] B = BitConverter.GetBytes(v);
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@int, new Call<int, Stream>((Stream stream) => {
                byte[] B = new byte[4];
                Utility.Read(stream, B, 4);
                return BitConverter.ToInt32(B, 0);
            }).Method);

            //uint
            _getBytes.Add(Reflect.@uint, new Exec<uint, Stream>((uint v, Stream stream) => {
                byte[] B = BitConverter.GetBytes(v);
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@uint, new Call<uint, Stream>((Stream stream) => {
                byte[] B = new byte[4];
                Utility.Read(stream, B, 4);
                return BitConverter.ToUInt32(B, 0);
            }).Method);

            //long
            _getBytes.Add(Reflect.@long, new Exec<long, Stream>((long v, Stream stream) => {
                byte[] B = BitConverter.GetBytes(v);
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@long, new Call<long, Stream>((Stream stream) => {
                byte[] B = new byte[8];
                Utility.Read(stream, B, 8);
                return BitConverter.ToInt64(B, 0);
            }).Method);

            //ulong
            _getBytes.Add(Reflect.@ulong, new Exec<ulong, Stream>((ulong v, Stream stream) => {
                byte[] B = BitConverter.GetBytes(v);
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@ulong, new Call<ulong, Stream>((Stream stream) => {
                byte[] B = new byte[8];
                Utility.Read(stream, B, 8);
                return BitConverter.ToUInt64(B, 0);
            }).Method);

            //float
            _getBytes.Add(Reflect.@float, new Exec<float, Stream>((float v, Stream stream) => {
                byte[] B = BitConverter.GetBytes(v);
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@float, new Call<float, Stream>((Stream stream) => {
                byte[] B = new byte[4];
                Utility.Read(stream, B, 4);
                return BitConverter.ToSingle(B, 0);
            }).Method);

            //double
            _getBytes.Add(Reflect.@double, new Exec<double, Stream>((double v, Stream stream) => {
                byte[] B = BitConverter.GetBytes(v);
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@double, new Call<double, Stream>((Stream stream) => {
                byte[] B = new byte[8];
                Utility.Read(stream, B, 8);
                return BitConverter.ToDouble(B, 0);
            }).Method);

            //decimal
            _getBytes.Add(Reflect.@decimal, new Exec<decimal, Stream>((decimal v, Stream stream) => {
                string _v = v.ToString();

                byte[] B = Encoding.ASCII.GetBytes(_v);
                byte[] len_B = BitConverter.GetBytes(B.Length);
                stream.Write(len_B, 0, 4);
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@decimal, new Call<decimal, Stream>((Stream stream) => {
                byte[] B = new byte[4];
                Utility.Read(stream, B, 4);
                int len_string = BitConverter.ToInt32(B, 0);
                B = new byte[len_string];
                Utility.Read(stream, B, len_string);
                return decimal.Parse(Encoding.ASCII.GetString(B));
            }).Method);

            //char
            _getBytes.Add(Reflect.@char, new Exec<char, Stream>((char v, Stream stream) => {
                byte[] B = BitConverter.GetBytes(v);
                byte[] len_B = BitConverter.GetBytes(B.Length);
                stream.Write(len_B, 0, 4);
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@char, new Call<char, Stream>((Stream stream) => {
                byte[] B = new byte[4];
                Utility.Read(stream, B, 4);
                int len_string = BitConverter.ToInt32(B, 0);
                Utility.Read(stream, B, len_string);
                return BitConverter.ToChar(B, 0);
            }).Method);

            //string

            _getBytes.Add(Reflect.@string, new Exec<string, Stream>((string v, Stream stream) => {
                if (v == null) {
                    stream.Write(B_NULL, 0, 1); return;
                }
                stream.Write(B_NOT_NULL, 0, 1);

                byte[] B = Encoding.UTF8.GetBytes(v);
                byte[] len_B = BitConverter.GetBytes(B.Length);
                stream.Write(len_B, 0, 4);
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@string, new Call<string, Stream>((Stream stream) => {
                byte[] B = new byte[4];
                Utility.Read(stream, B, 1);
                if (B[0] == 1) {
                    return null;
                }

                Utility.Read(stream, B, 4);
                int len_string = BitConverter.ToInt32(B, 0);
                B = new byte[len_string];
                Utility.Read(stream, B, len_string);
                return Encoding.UTF8.GetString(B);
            }).Method);

            //DateTime
            _getBytes.Add(Reflect.@DateTime, new Exec<DateTime, Stream>((DateTime v, Stream stream) => {
                byte[] B = BitConverter.GetBytes(v.ToUniversalTime().ToBinary());
                stream.Write(B, 0, B.Length);
            }).Method);
            _getValue.Add(Reflect.@DateTime, new Call<DateTime, Stream>((Stream stream) => {
                byte[] B = new byte[8];
                Utility.Read(stream, B, 8);
                return DateTime.FromBinary(BitConverter.ToInt64(B, 0)).ToLocalTime();
            }).Method);
        }
        #endregion

        #region GetBytes

        public static MethodInfo GetBytes(Type _t) {
            if (!_getBytes.ContainsKey(_t)) {
                return null;
            }

            return _getBytes[_t];
        }
        #endregion GetBytes

        public static readonly MethodInfo array_toString =
            new Exec<int[], Stream>(ToString<int>).Method.GetGenericMethodDefinition();
        static void ToString<V>(V[] array, Stream stream) {
            Exec<V, Stream> item_helper = BinHelper<V>.toStringHelper;
            for (int i = 0, len = array.Length; i < len; i++) {
                item_helper(array[i], stream);
            }
        }

        public static readonly MethodInfo array_parse =
            new Exec<int[], int, Stream>(Parse<int>).Method.GetGenericMethodDefinition();
        static void Parse<V>(V[] array, int length, Stream stream) {
            for (int i = 0; i < length; i++) {
                array[i] = BinHelper<V>.Parse(stream);
            }
        }

        public static readonly MethodInfo list_toString =
            new Exec<IList<int>, Stream>(ToString<int>).Method.GetGenericMethodDefinition();
        static void ToString<V>(IList<V> list, Stream stream) {
            Exec<V, Stream> item_helper = BinHelper<V>.toStringHelper;
            for (int i = 0, len = list.Count; i < len; i++) {
                item_helper(list[i], stream);
            }
        }

        public static readonly MethodInfo list_parse =
            new Exec<IList<int>, int, Stream>(Parse<int>).Method.GetGenericMethodDefinition();
        static void Parse<V>(IList<V> list, int length, Stream stream) {
            for (int i = 0; i < length; i++) {
                list.Add(BinHelper<V>.Parse(stream));
            }
        }

        public static readonly MethodInfo dict_toString =
            new Exec<IDictionary<int, int>, Stream>(ToString<int, int>).Method.GetGenericMethodDefinition();
        static void ToString<k, v>(IDictionary<k, v> dict, Stream jsonStream) {
            Exec<k, Stream> key_helper = BinHelper<k>.toStringHelper;
            Exec<v, Stream> value_helper = BinHelper<v>.toStringHelper;
            foreach (KeyValuePair<k, v> kv in dict) {
                key_helper(kv.Key, jsonStream);
                value_helper(kv.Value, jsonStream);
            }
        }

        public static readonly MethodInfo dict_parse =
            new Exec<IDictionary<int, int>, int, Stream>(Parse<int, int>).Method.GetGenericMethodDefinition();
        static void Parse<k, v>(IDictionary<k, v> dict, int len, Stream stream) {
            for (int i = 0; i < len; i++) {
                k key = BinHelper<k>.Parse(stream);
                v value = BinHelper<v>.Parse(stream);
                if (dict.ContainsKey(key)) {
                    dict[key] = value;
                } else {
                    dict.Add(key, value);
                }
            }
        }
    }
}
