﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using vJine.Core.IoC;

namespace vJine.Core.ORM.Adapters {
    public partial class SQLite : IDbAdapter<SQLite> {
        /// <summary>
        /// http://www.sqlite.org/datatype3.html
        /// http://system.data.sqlite.org/index.html/ci/ad2b42f3cc?sbs=0
        /// </summary>
        static void Init_TypeMap() {
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@bool, "BIT", DbType.Boolean);
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@sbyte, "INT8", DbType.SByte, typeof(TypeConverters.sbyte_));
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@byte, "UINT8", DbType.Byte);
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@short, "INT16", DbType.Int16);
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@ushort, "UINT16", DbType.UInt16, typeof(TypeConverters.ushort_));
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@int, "INT32", DbType.Int32);
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@uint, "UINT32", DbType.UInt32, typeof(TypeConverters.uint_));
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@long, "INT64", DbType.Int64);
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@ulong, "UINT64", DbType.UInt64, typeof(TypeConverters.ulong_));
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@float, "DOUBLE", DbType.Double, typeof(TypeConverters.float_double));
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@double, "DOUBLE", DbType.Double);
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@decimal, "DECIMAL", DbType.Decimal);

            OrmConfig<SQLite>.InitDefaultMap(Reflect.@char, "TEXT", DbType.AnsiStringFixedLength, typeof(TypeConverters.char_string));
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@string, "TEXT", DbType.AnsiString);
            OrmConfig<SQLite>.InitDefaultMap(Reflect.@DateTime, "Text", DbType.AnsiString, typeof(TypeConverters.datetime_string));

            OrmConfig<SQLite>.InitDefaultMap(Reflect.@byteArray, "BLOB", DbType.Binary, typeof(TypeConverters.bytes_));

            OrmConfig<SQLite>.InitDefaultMap(typeof(Guid), "GUID", DbType.Guid);
        }
    }
}
