﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace vJine.Core.ORM.Adapters {
    /// <summary>
    /// ACCESS 数据库适配器类型
    /// </summary>
    public partial class ACCESS : IDbAdapter<ACCESS> {
        static ACCESS() {
            ACCESS.Init_TypeMap();
            ACCESS.Init_Keywords();
        }
        /// <summary>
        /// 实例化ACCESS数据库适配器类型
        /// </summary>
        public ACCESS() {
            this.Name = Class<ACCESS>.Name;

            this.ParamPrefix = "@";
            this.GetDateTime = "Select now";

            this.QuoteOpen = "[";
            this.QuoteClose = "]";

            this.NULL = " NULL";
            this.NOT_NULL = " NOT NULL";

            this.KeyWords = ACCESS._KeyWords;
        }

        /// <summary>
        /// 生成删除数据库表命令
        /// </summary>
        /// <typeparam name="Tentity">实体类型</typeparam>
        /// <param name="IfExists">判断存在</param>
        /// <param name="table_name">表名</param>
        /// <returns>数据库指令</returns>
        public override DbCommand PrepareDrop<Tentity>(bool IfExists, string table_name) {

            DbCommand dbCmd = this.Conn.CreateCommand();
            dbCmd.CommandType = System.Data.CommandType.Text;

            dbCmd.CommandText = "DROP TABLE " + table_name;

            return dbCmd;
        }
    }
}
