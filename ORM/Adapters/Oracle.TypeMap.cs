﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using vJine.Core.IoC;

namespace vJine.Core.ORM.Adapters {
    public partial class Oracle : IDbAdapter<Oracle> {
        /// <summary>
        /// http://msdn.microsoft.com/en-us/library/yk72thhd.aspx
        /// http://docs.oracle.com/cd/B28359_01/appdev.111/b28395/oci03typ.htm#i429972
        /// </summary>
        static void Init_TypeMap() {

            OrmConfig<Oracle>.InitDefaultMap(Reflect.@bool, "CHAR(1)", DbType.AnsiStringFixedLength,  typeof(TypeConverters.bool_string));
            OrmConfig<Oracle>.InitDefaultMap(Reflect.@sbyte, "NUMBER(3)", DbType.Int16, typeof(TypeConverters.sbyte_short));
            OrmConfig<Oracle>.InitDefaultMap(Reflect.@byte, "NUMBER(3)", DbType.Int16, typeof(TypeConverters.byte_short));
            OrmConfig<Oracle>.InitDefaultMap(Reflect.@short, "NUMBER(5)", DbType.Int32, typeof(TypeConverters.short_int));
            OrmConfig<Oracle>.InitDefaultMap(Reflect.@ushort, "NUMBER(5)", DbType.Int32, typeof(TypeConverters.ushort_int));
            OrmConfig<Oracle>.InitDefaultMap(Reflect.@int, "NUMBER(10)", DbType.Int64, typeof(TypeConverters.int_long));
            OrmConfig<Oracle>.InitDefaultMap(Reflect.@uint, "NUMBER(10)", DbType.Int64, typeof(TypeConverters.uint_long));
            OrmConfig<Oracle>.InitDefaultMap(Reflect.@long, "NUMBER(19)", DbType.Decimal, typeof(TypeConverters.long_decimal));
            OrmConfig<Oracle>.InitDefaultMap(Reflect.@ulong, "NUMBER(20)", DbType.Decimal, typeof(TypeConverters.ulong_decimal));
            OrmConfig<Oracle>.InitDefaultMap(Reflect.@float, "BINARY_FLOAT", DbType.Single);
            OrmConfig<Oracle>.InitDefaultMap(Reflect.@double, "BINARY_DOUBLE", DbType.Double);
            OrmConfig<Oracle>.InitDefaultMap(Reflect.@decimal, "DECIMAL(33,3)", DbType.Decimal);

            OrmConfig<Oracle>.InitDefaultMap(Reflect.@char, "CHAR(1)", DbType.AnsiStringFixedLength, typeof(TypeConverters.char_string));
            OrmConfig<Oracle>.InitDefaultMap(Reflect.@string, "VARCHAR2(50)", DbType.AnsiString);
            OrmConfig<Oracle>.InitDefaultMap(Reflect.@DateTime, "TIMESTAMP", DbType.DateTime/*, typeof(OrmTypeConverter.DateTime_)*/);

            OrmConfig<Oracle>.InitDefaultMap(Reflect.@byteArray, "BLOB", DbType.Binary, typeof(TypeConverters.bytes_));

            OrmConfig<Oracle>.InitDefaultMap(typeof(Guid), "VARCHAR2(36)", DbType.AnsiString, typeof(TypeConverters.guid_string));
        }
    }
}
