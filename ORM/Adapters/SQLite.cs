﻿using System;
using System.Collections.Generic;
using System.Text;
using vJine.Core.IoC;
using System.Data.Common;

namespace vJine.Core.ORM.Adapters {
    /// <summary>
    /// SQLite 数据库适配器类型
    /// </summary>
    public partial class SQLite : IDbAdapter<SQLite> {

        static SQLite() {
            SQLite.Init_Keywords();
            SQLite.Init_TypeMap();
        }
        /// <summary>
        /// 实例化SQLite数据库适配器类型
        /// </summary>
        public SQLite() {
            this.Name = Class<SQLite>.Name;

            this.ParamPrefix = ":";
            this.GetDateTime = "Select datetime(CURRENT_TIMESTAMP,'localtime')";

            this.QuoteOpen = "\"";
            this.QuoteClose = "\"";

            this.NULL = "";
            this.NOT_NULL = " NOT NULL";

            this.KeyWords = SQLite._KeyWords;
        }
    }
}
