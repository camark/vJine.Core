﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using vJine.Core.IoC;

namespace vJine.Core.ORM.Adapters {
    /// <summary>
    /// Oracle 数据库适配器类型
    /// </summary>
    public partial class Oracle : IDbAdapter<Oracle> {

        static Oracle() {
            Oracle.Init_Keywords();
            Oracle.Init_TypeMap();
        }
        /// <summary>
        /// 实例化Oracle数据库适配器类型
        /// </summary>
        public Oracle() {
            this.Name = Class<Oracle>.Name;

            this.ParamPrefix = ":";
            this.GetDateTime = "select sysdate from dual";

            this.NULL = "";
            this.NOT_NULL = " NOT NULL";

            this.KeyWords = Oracle._KeyWords;
        }
        /// <summary>
        /// 生成删除数据库表命令
        /// </summary>
        /// <typeparam name="Tentity">实体类型</typeparam>
        /// <param name="IfExists">判断存在</param>
        /// <param name="table_name">表名</param>
        /// <returns>数据库指令</returns>
        public override DbCommand PrepareDrop<Tentity>(bool IfExists, string table_name) {

            table_name = table_name.ToUpper();

            DbCommand dbCmd = this.Conn.CreateCommand();
            dbCmd.CommandType = System.Data.CommandType.Text;
            if(IfExists) {
                dbCmd.CommandText = "declare tbl_exists number;" +
                                    "begin " +
                                    "select count(*) into tbl_exists from user_tables where table_name='" + table_name + "';" +
                                    "if tbl_exists > 0 then execute immediate 'drop table " + table_name + " purge'; end if;" +
                                    "end;";
            } else {
                dbCmd.CommandText = "DROP TABLE " + table_name;
            }

            return dbCmd;
        }
        /// <summary>
        /// 生成查询数据命令
        /// </summary>
        /// <typeparam name="Tentity">实体类型</typeparam>
        /// <param name="max">最大记录数</param>
        /// <param name="fields">属性集合（要查询的属性）</param>
        /// <param name="table_name">表名</param>
        /// <param name="where">条件表达式</param>
        /// <param name="orders">排序字段</param>
        /// <returns></returns>
        public override DbCommand PrepareQuery<Tentity>(int max, List<Class<Tentity>.Property> fields, string table_name, Class<Tentity>.Where where, List<Class<Tentity>.Property> orders) {
            int pCounter = 0;
            StringBuilder sbCmd = new StringBuilder();
            DbCommand dbCmd = this.Conn.CreateCommand();

            sbCmd.Append("Select");

            //Fields
            this.ToSelectString<Tentity>(dbCmd, sbCmd, fields);

            sbCmd.Append(" From ").Append(table_name);

            //Where
            if(where != null) {
                sbCmd.Append(" Where ");
                this.ToWhereString<Tentity>(
                    dbCmd, sbCmd, where, ref pCounter, false);
            }

            //Order By
            if(orders != null && orders.Count > 0) {
                sbCmd.Append(" Order By ");
                for(int j = 0, len_j = orders.Count - 1; j <= len_j; j++) {
                    Class<Tentity>.Property order_j = orders[j];
                    if(order_j.IsASC == null) {
                        continue;
                    }
                    sbCmd.Append(order_j.Name).Append(order_j.IsASC.Value ? " ASC" : " DESC");
                    if(j < len_j) {
                        sbCmd.Append(",");
                    }
                }
            }

            if (max > 0) {
                dbCmd.CommandText = "Select * From (" + sbCmd.ToString() + ") Where rownum <=" + max.ToString();
            } else {
                dbCmd.CommandText = sbCmd.ToString();
            }            
        
            return dbCmd;
        }
    }
}
