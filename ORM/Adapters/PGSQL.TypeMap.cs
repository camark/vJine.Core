﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using vJine.Core.IoC;

namespace vJine.Core.ORM.Adapters {
    public partial class PGSQL : IDbAdapter<PGSQL> {
        //http://www.postgresql.org/docs/9.2/static/datatype.html
        static void Init_TypeMap() {
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@bool, "boolean", DbType.Boolean);
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@sbyte, "smallint", DbType.Int16, typeof(TypeConverters.sbyte_short));
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@byte, "smallint", DbType.Int16, typeof(TypeConverters.byte_short));
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@short, "smallint", DbType.Int16);
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@ushort, "smallint", DbType.Int16, typeof(TypeConverters.ushort_short));
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@int, "integer", DbType.Int32);
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@uint, "integer", DbType.Int32, typeof(TypeConverters.uint_int));
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@long, "bigint", DbType.Int64);
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@ulong, "bigint", DbType.Int64, typeof(TypeConverters.ulong_long));
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@float, "real", DbType.Single);
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@double, "double precision", DbType.Double);
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@decimal, "decimal", DbType.Decimal);

            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@char, "char(1)", DbType.StringFixedLength, typeof(TypeConverters.char_string));
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@string, "varchar(50)", DbType.AnsiString);
            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@DateTime, "timestamp with time zone", DbType.DateTime);

            OrmConfig<PGSQL>.InitDefaultMap(Reflect.@byteArray, "bytea", DbType.Binary, typeof(TypeConverters.bytes_));

            OrmConfig<PGSQL>.InitDefaultMap(typeof(Guid), "uuid", DbType.Guid);
        }
    }
}
