﻿using System;
using System.Collections.Generic;
using System.Text;
using vJine.Core.IoC;
using System.Data;

namespace vJine.Core.ORM.Adapters {
    public partial class ACCESS : IDbAdapter<ACCESS> {
        //http://www.cnblogs.com/wenjl520/archive/2009/03/02/1401575.html
        static void Init_TypeMap() {
            //OrmConfig<ACCESS>.InitDefaultMap(Reflect.@bool, "bit", DbType.Boolean); //以bit存储时存储NULL值不可以用IS NULL判断
            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@bool, "varchar(1)", DbType.AnsiString, typeof(TypeConverters.bool_string));
            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@sbyte, "byte", DbType.Byte, typeof(TypeConverters.sbyte_byte));
            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@byte, "byte", DbType.Byte);
            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@short, "short", DbType.Int16);
            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@ushort, "short", DbType.Int16, typeof(TypeConverters.ushort_short));
            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@int, "integer", DbType.Int32);
            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@uint, "integer", DbType.Int32, typeof(TypeConverters.uint_int));
            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@long, "decimal(20)", DbType.Decimal, typeof(TypeConverters.long_decimal));
            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@ulong, "decimal(20)", DbType.Decimal, typeof(TypeConverters.ulong_decimal));
            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@float, "single", DbType.Single);
            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@double, "double", DbType.Double);
            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@decimal, "decimal(28)", DbType.Decimal); //无法存取极值，有效位数仅有28位

            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@char, "varchar(1)", DbType.AnsiString, typeof(TypeConverters.char_string));
            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@string, "varchar(50)", DbType.AnsiString);

            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@DateTime, "varchar(23)", DbType.AnsiString, typeof(TypeConverters.datetime_string /*yyyy-MM-dd HH:mm:ss.fff*/)); 
            //OrmConfig<ACCESS>.InitDefaultMap(Reflect.@DateTime, "DateTime", DbType.DateTime); //标准表达式中的数据类型不匹配

            OrmConfig<ACCESS>.InitDefaultMap(Reflect.@byteArray, "LONGBINARY", DbType.Binary, typeof(TypeConverters.bytes_));

            OrmConfig<ACCESS>.InitDefaultMap(typeof(Guid), "Guid", DbType.Guid);
        }
    }
}
