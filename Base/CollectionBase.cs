﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Reflection;
using System.Collections;
using vJine.Core.IoC;

namespace vJine.Core.Base {
    /// <summary>
    /// 实体类型列表集合基类
    /// </summary>
    /// <typeparam name="Tentity">实体类型</typeparam>
    [Serializable]
    public class CollectionBase<Tentity> : BindingList<Tentity>, INotifyPropertyChanged, IDisposable {
        /// <summary>
        /// 将实体类集合转换为实体类数组
        /// </summary>
        /// <param name="items">实体类集合</param>
        /// <returns>实体类数组</returns>
        public static implicit operator Tentity[](CollectionBase<Tentity> items) {
            if (object.Equals(items, null)) {
                return null;
            }

            Tentity[] tArray = new Tentity[items.Count];
            for (int i = 0; i < items.Count; i++) {
                tArray[i] = items[i];
            }

            return tArray;
        }

        /// <summary>
        /// 实例化实体类列表集合
        /// </summary>
        public CollectionBase() {
        }

        #region INotifyPropertyChanged 成员
        /// <summary>
        /// 属性改变事件
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// 属性改变通知
        /// </summary>
        /// <param name="pName">属性名</param>
        protected void NotifyPropertyChanged(String pName) {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(pName));
            }
        }

        #endregion

        #region User Custom
        /// <summary>
        /// 添加实体类集合项目到当前集合
        /// </summary>
        /// <param name="Items">实体类集合</param>
        /// <returns>当前集合</returns>
        public CollectionBase<Tentity> Add(CollectionBase<Tentity> Items) {
            foreach (Tentity item in Items) {
                base.Add(item);
            }

            return this;
        }

        /// <summary>
        /// 添加实体类数组到当前集合
        /// </summary>
        /// <param name="Items">实体类数组</param>
        /// <returns>当前集合</returns>
        public CollectionBase<Tentity> Add(Tentity[] Items) {
            foreach (Tentity item in Items) {
                base.Add(item);
            }

            return this;
        }
        /// <summary>
        /// 添加实体类实例到当前集合
        /// </summary>
        /// <param name="Item">实体类实例</param>
        /// <returns>当前集合</returns>
        public new CollectionBase<Tentity> Add(Tentity Item) {
            base.Add(Item);

            return this;
        }
        /// <summary>
        /// 添加非重复实体类实例到当前集合
        /// </summary>
        /// <param name="Item">实体类实例</param>
        /// <param name="comparer">判断重复代理</param>
        /// <returns>返回True则替换，False新增</returns>
        public bool Add(Tentity Item, Call<bool, Tentity, Tentity> comparer) {
            for (int i = 0; i < this.Count; i++) {
                if (comparer(Item, base[i])) {
                    base.RemoveAt(i); base.Add(Item);
                    return true;
                }
            }

            base.Add(Item); return false;
        }

        /// <summary>
        /// 判断是否包含指定实体类实例
        /// </summary>
        /// <param name="Item">实体类实例</param>
        /// <param name="comparer">判断重复代理</param>
        /// <returns></returns>
        public bool Contains(Tentity Item, Call<bool, Tentity, Tentity> comparer) {
            for (int i = 0; i < this.Count; i++) {
                if (comparer(Item, base[i])) {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region IDisposable 成员
        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose() {
            for (int i = 0; i < this.Count; i++) {
                this[i] = default(Tentity);
            }
            this.Clear();
        }

        #endregion
    }
}
